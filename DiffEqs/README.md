# Differential equations and Feynman integrals

## LiteRed
* https://arxiv.org/pdf/1310.1145.pdf
* http://www.inp.nsk.su/~lee/programs/LiteRed/

## Libra
* https://arxiv.org/pdf/2012.00279.pdf
* https://rnlee.bitbucket.io/Libra/

## Multiloop integrals in dimensional regularization made simple
* https://arxiv.org/pdf/1304.1806.pdf

## Harmonic polylogarithms
* https://arxiv.org/pdf/hep-ph/9905237.pdf
