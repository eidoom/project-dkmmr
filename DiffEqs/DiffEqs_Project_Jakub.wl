(* ::Package:: *)

Graphics[{{AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.28541666666666676`, 0.3194444444444444}, {0.2812500000000001, 0.6965277777777777}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.2805534234640178, 0.6944680716586983}, {0.6576423061644494, 0.6980979384095571}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.6534700901306846, 0.6986159644659157}, {1.0305589728311162`, 0.7022458312167745}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.661798341040964, 0.32490584114711735`}, {0.2889107068579525, 0.31900585799055525`}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{1.029164629693055, 0.3260841347318802}, {0.6520582968982529, 0.3259610293100893}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{1.0308379126301415`, 0.702783277012072}, {1.0325649354553392`, 0.3256808787419865}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.6}}], EdgeForm[{AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.6672938330600942, 0.6993112750337483}, {0.6696628105621323, 0.3222123631624946}}]}, {AbsoluteThickness[1], CapForm[Square], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.7000000000000001}}], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], {AbsoluteThickness[1], CapForm[Square], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], {AbsoluteThickness[1], CapForm[Square], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], Arrow[{{1.1250000000000002`, 0.21111111111111092`}, {1.0333333333333334`, 0.3277777777777775}}]}}}}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.7000000000000001}}], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], Arrow[{{1.1395833333333336`, 0.8006944444444445}, {1.0312500000000002`, 0.704861111111111}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.7000000000000001}}], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.1729166666666667, 0.8006944444444445}, {0.28125000000000006`, 0.6944444444444443}}]}, {AbsoluteThickness[1], CapForm[Square], Arrowheads[{{0.04, 0.7000000000000001}}], EdgeForm[{GrayLevel[0.], Opacity[1.], AbsoluteThickness[1], CapForm[Square]}], Arrow[{{0.15833333333333335`, 0.20486111111111105`}, {0.2854166666666667, 0.3194444444444443}}]}, Inset[Null, {0.4375000000000001, 0.2861111111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.4291666666666668, 0.23611111111111105`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7875000000000002, 0.8694444444444446}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7875000000000002, 0.5902777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.9916666666666669, 0.6694444444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.43125000000000013`, 0.2798611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.4666666666666668, 0.2548611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k1", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.44791666666666674`, 0.2715277777777777}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k2", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.8187500000000001, 0.2798611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k1-p1", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.15208333333333338`, 0.4965277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k1-p1-p2", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.40000000000000013`, 0.7444444444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k2-p1-p2", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.7645833333333335, 0.7527777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.054166666666667, 0.5152777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.1020833333333335`, 0.5215277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k2-p1-p2-p3", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {1.0625000000000002`, 0.5048611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.6958333333333335, 0.5069444444444444}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8229166666666667, 0.46319444444444446`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.6958333333333335, 0.5069444444444444}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7770833333333335, 0.4819444444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["k1-k2", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.6958333333333335, 0.4965277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["p3", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {1.147916666666667, 0.7965277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["p4", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {1.1395833333333336`, 0.19861111111111107`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["p2", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.12708333333333335`, 0.8069444444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["p1", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.11666666666666667`, 0.17361111111111105`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.3437500000000001, 0.18819444444444444`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.0125000000000002`, 0.39027777777777783`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7854166666666669, 0.5881944444444445}, {Left, Top}, {0.054166666666666696`, Automatic}, Alignment -> {Left, Top}], Inset[Null, {0.7854166666666669, 0.5881944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7854166666666669, 0.5881944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7854166666666669, 0.5881944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.7854166666666669, 0.5881944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.9854166666666668, 0.5861111111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["I", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.45000000000000007`, 0.3652777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.3270833333333334, 0.5131944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.3125, 0.5027777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.32500000000000007`, 0.5069444444444444}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["II", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.3041666666666667, 0.5048611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.45416666666666683`, 0.6611111111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.45416666666666683`, 0.6465277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["III", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.45416666666666683`, 0.6340277777777779}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8250000000000002, 0.6590277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8291666666666668, 0.6465277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8020833333333335, 0.6361111111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["V", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.9812500000000002, 0.5027777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8208333333333335, 0.6548611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8145833333333335, 0.6569444444444446}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["VI", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.8250000000000002, 0.36944444444444446`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.6000000000000001, 0.5048611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["VII", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.5875000000000001, 0.5048611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8145833333333335, 0.6506944444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8145833333333335, 0.6298611111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8145833333333335, 0.6173611111111111}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8250000000000002, 0.6444444444444445}, {Left, Top}, {0.029166666666666785`, Automatic}, Alignment -> {Left, Top}], Inset[Null, {0.8416666666666668, 0.6590277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.8937500000000003, 0.6402777777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["IV", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.8145833333333335, 0.6465277777777778}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.2354166666666668`, 0.7361111111111112}, {Left, Top}, {0.025000000000000133`, Automatic}, Alignment -> {Left, Top}], Inset[Null, {0.23750000000000002`, 0.7902777777777779}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.24375000000000005`, 0.8861111111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell[Style["  Two-loop massless box ", FontSize -> 24, FontColor -> GrayLevel[0.]], GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.2270833333333334, 0.9194444444444445}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.2770833333333336`, 0.6611111111111112}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.25, 0.0756944444444444}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.13958333333333336`, 0.050694444444444375`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.28125, 0.061111111111111005`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[TextCell["The blue Roman numerals indicate the position of\nthe propagator in the 'props' list.", GeneratedCell -> False, CellAutoOverwrite -> False, CellBaseline -> Baseline, TextAlignment -> Left], {0.2041666666666667, 0.06944444444444442}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {1.179166666666667, 0.15486111111111112`}, {Left, Baseline}, Alignment -> {Left, Top}], Inset[Null, {0.5312500000000001, 0.3055555555555557}, {Left, Baseline}, Alignment -> {Left, Top}]}, ContentSelectable -> True, ImagePadding -> {{0., 0.}, {0., 0.}}, ImageSize -> {480, 360}, PlotRange -> {{0., 1.3333333333333335`}, {0., 1.}}, PlotRangePadding -> Automatic]


(* ::Section::Closed:: *)
(*Create the basis*)


<<LiteRed`;(*Loading the package*)
SetDirectory[NotebookDirectory[]];
(*Setting working directory*)
SetDim[d];(*d stands for the dimensionality*)
Declare[{k1,k2,p1,p2,p3},Vector];
props = {k1,k1-p1,k1-p1-p2,k2-p1-p2,k2-p1-p2-p3,k2,k1-k2};


sp[p1,p1]=0;
sp[p2,p2]=0;
sp[p3,p3]=0;
sp[p1,p2]=s/2;
sp[p2,p3]=t/2;
sp[p1,p3]=-(s+t)/2;


NewBasis[boxtwoloops,props,{k1,k2},Directory->"2L_box_basis", Append->True];


AnalyzeSectors[boxtwoloops, {___,0,0}]


(* what does EMs do here? *)
FindSymmetries[boxtwoloops,EMs->True]


SolvejSector/@UniqueSectors[boxtwoloops]


MIs[boxtwoloops]


(* ::Section::Closed:: *)
(*Load the basis*)


Quit[]


<<LiteRed`;(*Loading the package*)
SetDirectory[NotebookDirectory[]];
(*Setting working directory*)
SetDim[d];(*d stands for the dimensionality*)
Declare[{k1,k2,p1,p2,p3},Vector]
<<"2L_box_basis/boxtwoloops";(*Load the basis*)


ExecuteDefinitions[boxtwoloops]


(* ::Section:: *)
(*Generate a system of diff eqs*)


MIs[boxtwoloops]


ds[x_,y_] := 1/2*Collect[IBPReduce[Dinv[MIs[boxtwoloops],sp[x,y]]],_j,Factor]
ds12 = ds[p1,p2];
ds23 = ds[p2,p3];
ds13 = ds[p1,p3];


(* returns the mass dimension of a given MI *)
2*Collect[sp[p1,p2]*ds12+sp[p2,p3]*ds23+sp[p1,p3]*ds13,_j,Factor]


(* we can check whether the differentiation of MIs makes sense by considering their topology *)
Select[Position[ds12,0],Length[#]==1&] // Flatten (* find the index of the MI that differentiates to 0 *)
MIs[boxtwoloops][[%]] /. j[boxtwoloops,x__]:>List[x] // Flatten (* extract the propagator powers of this MI *)


(* ::Text:: *)
(*We leave only three propagators and pinch all the others. This leads to the t-channel bubble:Graphics[{{AbsoluteThickness[1], CapForm[Square], Circle[{0.6645833333333335, 0.49065695529513986`}, {0.12291666666666667`, 0.23750000000000027`}]}, {AbsoluteThickness[1], CapForm[Square], Line[{{0.6625000000000002, 0.7281569552951399}, {0.5312500000000002, 0.8594069552951402}}]}, {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], Line[{{0.6616440364495355, 0.7273807535462321}, {0.802466518674806, 0.8483028503685808}}]}}}, {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], Line[{{0.7958333333333335, 0.1219069552951404}, {0.6645833333333335, 0.2531569552951407}}]}}}, {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], Line[{{0.5213662586717577, 0.13293630910178822`}, {0.6621887408970282, 0.2538584059241369}}]}}}}}, {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], {AbsoluteThickness[1], CapForm[Square], Line[{{0.6662490322747661, 0.2498000999856106}, {0.6626027358600046, 0.7254514316335162}}]}}}}}}, ContentSelectable -> True, ImagePadding -> {{0., 0.}, {0., 0.}}, ImageSize -> {181.5, Automatic}, PlotRange -> {{0., 1.3333333333333335`}, {0., 1.}}, PlotRangePadding -> Automatic]*)
(*So it is not suprising that the derivative with respect to s12 vanishes.*)


(* change the differential operators from dsij to {ds,dt} *)
ds = Collect[ds12-ds13, _j, Factor];
dt = Collect[ds23-ds13, _j, Factor];
(* we can again check the mass dimension of a given MI *)
Collect[s*ds+t*dt, _j, Factor] 


(* change the differential operators from dsij to {d\[Sigma],dx} *)
dsig = Collect[-ds-x*dt /. {s:>-sig, t:>-x*sig}, _j, Factor];
dx = Collect[-sig*dt /. {s:>-sig, t:>-x*sig}, _j, Factor];
(* again, returns the mass dimension of a given MI *)
matXEPS = Table[Coefficient[dx[[i]], MIs[boxtwoloops][[j]]], {i,8},{j,8}] /. {sig:>1, d:>4-2*eps} // Simplify


(* ::Section:: *)
(*Solve this system*)


(* ::Text:: *)
(*Before we tackle the complicated 2L matrix above, let's try to reproduce what happens with the equivalent matrix at 1L level:*)


(* ::Subsection:: *)
(*1L box example*)


(* ::Subsubsection:: *)
(*Libra*)


(*Quit[]*)


(* the workflow below is adapted from Tutorial 1 attached to Libra *)
<<Libra`


(* from Duhr's lecture 2 *)
matXEPS = {{(d-4)/(2x),0,0}, {0,0,0}, {2*(d-3)/(x^2*(x+1)), -2*(d-3)/(x*(x+1)), -(-d+2x+6)/(2x*(x+1))}} /.d:>4-2*eps;


matXEPS//MatrixForm


NewDSystem[dsbox1L, x->matXEPS]


t = VisTransformation[dsbox1L, x, eps, Animate->{{7}\[LongLeftRightArrow]{8}}] (* I think other reduction sequences are also possible *)


Transform[dsbox1L,t]


FuchsianQ[dsbox1L,x] (* if this is not true, we can't factor out \[Epsilon] *)


Block[{\[Mu]=1,C},C[1]=1;C[_]=0;
t=FactorOut[dsbox1L[x],x,eps,\[Mu]]]
Factor[Det[t]]=!=0


(* this is the \[Epsilon]-free matrix Mf *)
(* it's equivalent to GaugeTransform[matXEPS,T,x]//Factor , where T is the overall transformation matrix(see below) *)
Transform[dsbox1L,t]


EFormQ[dsbox1L[x],eps]


(* for this particular case, the canonical form is simple and this line doesn't really achieve anything useful *)
(* t=Transpose@JDSpace[SeriesCoefficient[dsbox1L[x]/eps,{x,0,-1}]]
Transform[dsbox1L,t] *)


transformation=OverallTransformation[dsbox1L]; (* returns an association *)
(* we can retrieve any information we need in the following way *)
Mi=transformation[In][x](* initial matrix *)
Mf=transformation[Out][x];(* transformed matrix *)
T=transformation[Transform];(* overall transformation matrix *)
(* Check that everything is Ok *) 
Mi==matXEPS&&Mf==dsbox1L[x]&&Factor[Transform[matXEPS,T,x]]==Factor[Mf]


(* convert the canonical result to a function so that we can do iterated integrals below *)
canonicalM[x_] := Evaluate[Mf]
canonicalM[xx]


(* ::Subsubsection::Closed:: *)
(*Path ordered exponential*)


(* eps^0 *)
Pexp[0] = IdentityMatrix[3];


(* why are these integrals indefinite, rather than from some x0 to x? *)


(* eps^1 *)
Pexp[1] = Integrate[canonicalM[x],x]


(* eps^2 *)
canonicalM[x1].canonicalM[x2];
Pexp[2] = Integrate[Integrate[%,x2]/.x2:>x1 ,x1]/.x1:>x;


(* eps^3 *)
canonicalM[x1].canonicalM[x2].canonicalM[x3];
Pexp[3] = Integrate[Integrate[Integrate[%,x3]/.x3:>x2, x2]/.x2:>x1, x1]/.x1:>x;


GeneralSolution3 = Sum[Pexp[i],{i,0,3}];
D[GeneralSolution3, x] - canonicalM[x].GeneralSolution3;
Series[%,{eps,0,3}]//MatrixForm


(* ::Subsubsection::Closed:: *)
(*Initial condition*)


(* bubble integral evaluates to Subscript[c, \[CapitalGamma]](-s)^-\[Epsilon]/(\[Epsilon](1-2\[Epsilon])) *)
(* we must have (-s)^-\[Epsilon] by the equation on slide 6: I(s)=s^([I]/2)I[scaleless], where [I]=LD-[N]-2\[Nu]=4-2\[Epsilon]-0-2*2=-2\[Epsilon] *)
cGamma=Exp[EulerGamma*eps]Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
Series[(-s)^(-eps)*cGamma/(eps*(1-2*eps)),{eps,0,0}]
(* note that this expression is consistent with the one form QCDLoop, up to O[\[Epsilon]] *)
Series[(-s)^(-eps)*(1/eps+2), {eps,0,0}]


(* I don't get how the bubbles evaluate only to Subscript[c, \[CapitalGamma]] on the initial conditions *)
(* what happens to (-s)^-\[Epsilon]/(\[Epsilon]*1-2\[Epsilon])) ? *)
InitialConds = {cGamma,cGamma,Total@Table[c[i]*eps^i, {i,0,3}]}


GeneralBoxSol = Series[GeneralSolution3.InitialConds, {eps,0,3}];


(* general solution for the box: *)
GeneralBoxSol[[3]]
(* the box doesn't have a singularity in the u-channel, i.e. u=-s-t=-s(1+x)\[NotEqual]0, so we must enforce x\[NotEqual]0 *)
GeneralBoxSol = GeneralBoxSol /. Log[x]:>Log[-x]+I*Pi; (* analytical continuation *)
(* systematically fix c[i] by imposing no singularity in the u-channel, i.e. no Log[1+x] terms *)
series = Series[GeneralBoxSol[[3]],{x,-1,0},Assumptions->-1<x<0];
Coefficient[series,eps,0]
Coefficient[series,eps,1]//Simplify
Coefficient[series/.c[0]:>-4,eps,2]//Simplify
Coefficient[series/.{c[0]:>-4,c[1]:>0},eps,3]//Simplify


(* plug those values back into the vector I to get the answers for MIs *) 
GeneralSolution3.InitialConds/.{c[0]:>-4,c[1]:>0,c[2]:>4*Pi^2/3}//Simplify;
masters = Collect[Series[%,{eps,0,2}],eps,Simplify] (* why is the box off by an overall '-' sign??? *)
Series[T.%,{eps,0,2}]//Simplify (* this should work, but it doesn't *)


(* in our notation, matXEPS corresponds to Duhr's A, and T corresponds to his M - it's the matrix wihch allows us *)
(* to perform a gauge transformation and bring matXEPS/A into the canonical form Mf/A' *)


(* check that what Libra does to matXEPS is a gauge transformation by the overall matrix T *)
T (* the gauge transformation matrix *)
GaugeTransform[A_,T_,x_]:=Inverse[T].(A.T-D[T,x])
Factor[GaugeTransform[matXEPS,T,x]] (* this gives the canonical Mf/A' *)
%===Mf
D[masters,x]-Mf.masters // Simplify (* why doesn't this work? *)



