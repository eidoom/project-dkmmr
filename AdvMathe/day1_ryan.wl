(* ::Package:: *)

(* ::Section:: *)
(*One*)


g1=Graph[{"A"\[UndirectedEdge]"B","B"\[UndirectedEdge]"C","C"\[UndirectedEdge]"A","C"\[UndirectedEdge]"D","A"\[UndirectedEdge]"D"},EdgeWeight->{3,7,11,28,44},EdgeLabels->"EdgeWeight",VertexLabels->"Name"]


one[graph_, vertices_]:=Total[AnnotationValue[{g1,#[[1]]<->#[[2]]},EdgeWeight]&/@Partition[vertices,2,1]];


one[g1,{"A","B","C","A"}]


(* ::Section:: *)
(*Two*)


g2=Graph[{"A"\[UndirectedEdge]"B","A"\[UndirectedEdge]"C","A"\[UndirectedEdge]"D"},VertexLabels->"Name"]


two[graph_,choose_]:=Module[
{outer,g3,new,l},
If[VertexDegree[graph,choose]!=3,Print["Must choose valence 3 node"];Return[graph]];
outer=VertexOutComponent[graph,{choose},{1}];
l=Length[outer];
new=Unique[choose]&/@Range[l];
g3=VertexDelete[graph,choose];
Do[
	g3=EdgeAdd[VertexAdd[g3,new[[i]]],{new[[i]]<->new[[Mod[i+1,l,1]]],new[[i]]<->outer[[i]]}],
	{i,Length[outer]}
];
g3
];


two[g2,"A"]
