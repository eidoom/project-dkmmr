(* ::Package:: *)

(* ::Chapter:: *)
(*Centrality*)


(* ::Section:: *)
(*1*)


g=NestGraph[Import[#,"Hyperlinks"][[;;UpTo[25]]]&,"https://www.wolfram.com",2]


(* ::Section:: *)
(*2*)


ranked=VertexList[g][[Ordering[PageRankCentrality[g],10,Greater]]]


(* ::Section:: *)
(*3*)


WebImage[ranked[[1]]]


(* ::Section:: *)
(*4*)


pgn=ResourceData["Power Grid Network"]


n=Round[Length[VertexList[pgn]]/100] (* 1% *)


GraphicsRow[HighlightGraph[pgn, 
Style[VertexList[pgn][[Ordering[#[pgn], n, Greater]]],{Red,Magnification->2}]
]&/@{DegreeCentrality,PageRankCentrality}]


(* ::Section:: *)
(*5*)


critnetwork[graph_, n_, depth_] :=
 Module[{edges, vertices, critical},
  vertices=VertexList[graph][[Ordering[PageRankCentrality[graph], All, Greater]]][[;;n]];
  critical=vertices;
  edges={};
  Do[
   Do[
     edges =
      Join[edges, EdgeList[graph, vertices[[i]] \[UndirectedEdge] _]];
     vertices =
      Join[vertices,
       EdgeList[graph, vertices[[i]] \[UndirectedEdge] _] /.
        x_ \[UndirectedEdge] y_ :>
         First@Select[{x, y}, # != vertices[[i]] &]],
     {i, Length[vertices]}
      ];
   ,
   {j, depth}];
  Return[DeleteDuplicates /@ {critical, edges, vertices}];
  ]

{critical, links, neighbours} = critnetwork[pgn, 5, 3];
HighlightGraph[pgn, {Style[Join[links, neighbours], Green],
  Style[critical, Red, Magnification -> 3]}]


(* ::Chapter:: *)
(*Finance*)
